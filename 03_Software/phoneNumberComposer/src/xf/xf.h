//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                   
// Class:            XF
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      Execution framework, which receives all pending events,manages them and passes them on to the proper target     
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <zephyr.h>
#include "event.h"

#ifndef XF_ONCE
#define XF_ONCE


using namespace std;

class XF
{
public:
    void init();
    static XF* getInstance(); 
    void pushEvent(Event* e);
    void execute();
    void unscheduleTM(Event* e);
private:
    XF(/* args */);         
    ~XF();                  
    Event* popEvent();
    void scheduleTM(Event* e);
    static void onTimeout(struct k_timer* t);
    static void onStop(struct k_timer* t);
    static XF theXF;        

    struct k_msgq queue;
    char __aligned(4) queue_buffer[30 * sizeof(Event*)];
};

#endif