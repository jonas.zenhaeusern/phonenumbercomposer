#ifndef IFONAOBSERVER_ONCE
#define IFONAOBSERVER_ONCE
#include <string>


class IFonaObserver
{
    public:
    virtual void onResponse(char* message) = 0;
};

#endif