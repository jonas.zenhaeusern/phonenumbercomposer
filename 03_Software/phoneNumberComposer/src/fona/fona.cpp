#include "fona.h"
#include "..\factory\factory.h"

Fona Fona::theFona;

Fona::Fona()
{
	state = ST_INIT;
	ev.setTarget(this);
	ev.setDnd(1);
}

Fona::~Fona()
{
}

void Fona::init()
{
    uart = Factory::uart();
}

void Fona::startBehaviour()
{
    ev.setId(evInitial);
	XF::getInstance()->pushEvent(&ev);
}


Fona* Fona::getInstance()
{
    return &theFona;
}

void Fona::send(string message){
        message += "\r\n";
        uart->uartSend(message.c_str());
}

void Fona::subscribe(IFonaObserver* subscriber)
{
    vector<IFonaObserver*>::iterator it;
    it = find(subscribers.begin(), subscribers.end(),subscriber);
    if (it == subscribers.end())
    {
        subscribers.push_back(subscriber);
    }
}

void Fona::notify()
{
    vector<IFonaObserver*>::iterator it;
    for (it=subscribers.begin(); it!=subscribers.end();++it)
    {
        (*it)->onResponse((char*)data);
    }
}

void Fona::onMessage(k_msgq* messages){  
	//assign received fona data to event 
    k_msgq_get(messages, &data, K_NO_WAIT);
    if(!(strncmp(data, "\r\n",2)==0)){
		//printk("%s\n",(char*)data);
        if(strcmp((char*)data, "OK\r\n")==0)
        {
            ev.setId(evOk);
        }
        else if(strcmp((char*)data, "RING\r\n")==0)
        {
            ev.setId(evRing);
        }
        else if(strcmp((char*)data, "ERROR\r\n")==0)
        {
            ev.setId(evError);
        }
        else if(strcmp((char*)data, "VOICE CALL: BEGIN\r\n")==0)
        {
            ev.setId(evVoiceCallBeginn);
        }
		else if(strcmp((char*)data, "PB DONE\r\n")==0)
        {
            ev.setId(evPBDone);
        }
        else if(strcmp((char*)data, "NO CARRIER\r\n")==0)
        {
            ev.setId(evNoCarrier);
        }
        else if(strcmp((char*)data, "VOICE CALL: END: 000008\r\n")==0)
        {
            ev.setId(evNoCarrier);
        }
		else if(strcmp((char*)data, "VOICE CALL: END: 000003\r\n")==0)
        {
            ev.setId(evNoCarrier);
        }
		else
		{
			ev.setId(evDefault);
		}
		ev.setTarget(this);
		XF::getInstance()->pushEvent(&ev);
		notify();
    }
}

void Fona::toggleAutoCall()
{
    if(autoCall)
    {
        autoCall = false;
        Factory::ledYellow()->off();
    }
    else
    {
        autoCall = true;
        Factory::ledYellow()->on();
    } 
}

void Fona::hangUp()
{
    ev.setId(evHangup);
    ev.setTarget(this);
    XF::getInstance()->pushEvent(&ev);
}

void Fona::initCall()
{
	ev.setId(evInitCall);
    ev.setTarget(this);
    XF::getInstance()->pushEvent(&ev);
}

Fona::FONASTATES Fona::getFonaState()
{
    return this->state;
}

bool Fona::getAutoCallStatus()
{
    return autoCall;
}

bool Fona::processEvent(Event* e)
{
    FONASTATES oldstate;

	oldstate = this->state;
	//manage fona state 
	switch (this->state)
	{
		case ST_INIT:
		{
			if(e->getId() == evInitial)
			{
				this->state = ST_CONFIG;
			}
			break;
		}
		case ST_CONFIG:
		{
			if(e->getId() == evOk)
			{
				this->state = ST_READY;
			}
            else if(e->getId() == evPBDone)
			{
				this->state = ST_READY;
			}
            else if(e->getId() == evError)
			{
				this->state = ST_ERROR;
			}
			break;
		}
		case ST_READY:
		{
			if(e->getId() == evRing)
			{
				this->state = ST_RING;
			}
            else if(e->getId() == evInitCall)
			{
				this->state = ST_INITCALL;
			}
            else if(e->getId() == evError)
			{
				this->state = ST_ERROR;
			}
			break;
		}
		case ST_RING:
		{			
			if(e->getId() == evVoiceCallBeginn)
			{
				this->state = ST_CALL;
			}
			else if(e->getId() == evHangup)
			{
				this->state = ST_READY;
			}
			else if(e->getId() == evError)
			{
				this->state = ST_ERROR;
			}
			break;
		}
		case ST_INITCALL:
		{
			if(e->getId() == evHangup)
			{
				this->state = ST_READY;	
			}
			else if(e->getId() == evVoiceCallBeginn)
			{
				this->state = ST_CALL;	
			}
			else if(e->getId() == evError)
			{
				this->state = ST_ERROR;
			}
			break;
		}
        case ST_CALL:
		{
			if(e->getId() == evNoCarrier)
			{
				this->state = ST_READY;
			}
            else if(e->getId() == evHangup)
			{
				this->state = ST_READY;
			}
            else if(e->getId() == evError)
			{
				this->state = ST_ERROR;
			}
			break;
		}
		case ST_ERROR:
		{
			if(e->getId() == evReset)
			{
				this->state = ST_CONFIG;
			}
			break;
		}
	}
	if(oldstate != this->state)
	{
        //action on entry
		switch (this->state)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_CONFIG:
			{
				send("ati");
				break;
			}
			case ST_READY:
			{
				break;
			}
			case ST_RING:
			{
				printk("incoming call\n");
				break;
			}
			case ST_CALL:
			{
				printk("call active\n");
				break;
			}
			case ST_INITCALL:
			{
				break;
			}
            case ST_ERROR:
			{
				printk("an error occured, check fona\n");
				break;
			}
		}
		//action on exit
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_CONFIG:
			{
				printk("ready\n");
				break;
			}
			case ST_READY:
			{
				break;
			}
			case ST_RING:
			{
				break;
			}
			case ST_CALL:
			{
				printk("call ended\n");
				break;
			}
			case ST_INITCALL:
			{
				break;
			}
            case ST_ERROR:
			{
				printk("an error occured, check fona\n");
				break;
			}
		return true;
	}
	return false;
	}
}
