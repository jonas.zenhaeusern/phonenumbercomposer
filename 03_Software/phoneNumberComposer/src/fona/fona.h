///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      reactive class that is used as a driver for the modem      
//
///////////////////////////////////////////////////////////////////////////////
#ifndef FONA_ONCE
#define FONA_ONCE

#include "../hw/uart.h"
#include "../xf/event.h"
#include "ifonaobserver.h"
#include "../xf/ireactive.h"
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class Uart;

class Fona : public Uart::IUARTObserver, public IReactive
{
    public:
        bool processEvent(Event* e); //contains statemachine
        void startBehaviour();  //start statemachine
        void hangUp();  //send hangup event
        void initCall(); //send initCall event
        static Fona* getInstance();
        void init();
        void subscribe(IFonaObserver* subscriber);
        void send(string command);  //send string to uart
        void toggleAutoCall();
        bool getAutoCallStatus();

                typedef enum FONASTATES {
            ST_INIT, 
            ST_CONFIG, 
            ST_READY, 
            ST_RING, 
            ST_CALL, 
            ST_INITCALL, 
            ST_ERROR
        } FONASTATES;

    FONASTATES getFonaState();
    private:
        FONASTATES state;   //current state of statemachine
    	typedef enum fonaEvID {     //list of all available events
		    evInitial
		    ,evOk
            ,evDefault
            ,evRing
            ,evVoiceCallBeginn
            ,evNoCarrier
            ,evHangup
            ,evInitCall
            ,evPBDone
            ,evError
            ,evReset
	    } fonaEvID;

        Event ev;
        bool autoCall;
        Fona();
        ~Fona();
        void notify();
        void onMessage(k_msgq* messages) override;
        static Fona theFona;
        Uart* uart;
        vector<IFonaObserver*> subscribers; //observer list
        char data[MAXDATASIZE]; 
};

#endif