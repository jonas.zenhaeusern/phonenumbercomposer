#include "model.h"

Model Model::theModel;

Model::Model()
{
    tempKeyInput = "";
    currentInput = "";
    //map button coordinates and click duration to string
    keyMap.insert(pair<string, string>("A,1,1", "1"));
    keyMap.insert(pair<string, string>("A,2,1", "2"));
    keyMap.insert(pair<string, string>("A,3,1", "3"));
    keyMap.insert(pair<string, string>("A,4,1", "A"));
    keyMap.insert(pair<string, string>("B,1,1", "4"));
    keyMap.insert(pair<string, string>("B,2,1", "5"));
    keyMap.insert(pair<string, string>("B,3,1", "6"));
    keyMap.insert(pair<string, string>("B,4,1", "B"));
    keyMap.insert(pair<string, string>("C,1,1", "7"));
    keyMap.insert(pair<string, string>("C,2,1", "8"));
    keyMap.insert(pair<string, string>("C,3,1", "9"));
    keyMap.insert(pair<string, string>("C,4,1", "C"));
    keyMap.insert(pair<string, string>("D,1,1", "*"));
    keyMap.insert(pair<string, string>("D,2,1", "0"));
    keyMap.insert(pair<string, string>("D,3,1", "#"));
    keyMap.insert(pair<string, string>("D,4,1", "+"));
    //LongClicks
    keyMap.insert(pair<string, string>("A,4,2", "A_LONG"));
    keyMap.insert(pair<string, string>("B,4,2", "B_LONG"));
    keyMap.insert(pair<string, string>("C,4,2", "C_LONG"));
    keyMap.insert(pair<string, string>("D,4,2", "D_LONG"));
}

Model::~Model()
{

}

Model* Model::getInstance()
{
    return &theModel;
}

string Model::getKeyMap(string _key)
{
    return keyMap[_key];
}

string Model::getCurrentInput()
{
    return this->currentInput;
}

string Model::getTempKeyInput()
{
    return this->tempKeyInput;
}

void Model::setCurrentInput(string _currentInput)
{
    this->currentInput = _currentInput;
}

void Model::setTempKeyInput(string _tempKeyInput)
{
    this->tempKeyInput = _tempKeyInput;
}