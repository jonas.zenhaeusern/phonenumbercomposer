///////////////////////////////////////////////////////////////////////////////
//                   
// Class:             
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:           
//
///////////////////////////////////////////////////////////////////////////////

#ifndef MODEL_ONCE
#define MODEL_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include <drivers/gpio.h>
#include <stdlib.h>
#include "../gpio/gpi.h"
#include <list>
#include <string>
#include <bits/stdc++.h>
#include <iostream>
#include <iterator>

class Model
{
    public:
        
        string getCurrentInput();
        string getTempKeyInput();
        void setCurrentInput(string _currentInput);
        void setTempKeyInput(string _tempKeyInput);
        static Model* getInstance();
        string getKeyMap(string _key);
    private:
        Model();
        ~Model();
        static Model theModel;
        string currentInput;    //all user inputs
        string tempKeyInput;    //last button input
        map<string, string> keyMap;
        
};
#endif