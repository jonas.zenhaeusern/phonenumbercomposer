#include "menu.h"

Menu Menu::theMenu;

Menu::Menu()
{

}

Menu::~Menu()
{

}

void Menu::init()
{
    state = ST_INIT;
	ev.setTarget(this);
	ev.setDnd(1);
}

Menu* Menu::getInstance()
{
    return &theMenu;
}

void Menu::startBehaviour()
{
	ev.setId(evInitial);
	XF::getInstance()->pushEvent(&ev);
}

bool Menu::processEvent(Event* e)
{
    MENUSTATES oldstate;

	oldstate = this->state;
	switch (this->state)
	{
        case ST_INIT:
		{
			if(e->getId() == evInitial)
			{
				this->state = ST_MENU;
			}
			break;
		}
        case ST_MENU:
		{
			if(e->getId() == evSMS)
			{
				this->state = ST_SMS;
			}
            if(e->getId() == evCall)
			{
				this->state = ST_CALL;
			}
			break;
		}
        case ST_SMS:
		{
			if(e->getId() == evMenu)
			{
				this->state = ST_MENU;
			}
			break;
		}
        case ST_CALL:
		{
			if(e->getId() == evMenu)
			{
				this->state = ST_MENU;
			}
			break;
		}
    }
    if(oldstate != this->state)
	{
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_MENU:
			{
				break;
			}
			case ST_CALL:
			{
				break;
			}
			case ST_SMS:
			{
				break;
			}
		}
        //on entry
		switch (this->state)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_MENU:
			{
				displayMenu();
				break;
			}
			case ST_CALL:
			{
				break;
			}
			case ST_SMS:
			{
				break;
			}
		}
		return true;
	}

}

MENUSTATES Menu::getMenuState()
{
	return this->state;
}

void Menu::displayMenu()
{
    printk("|----------|----------|\n|  A:CALL  |  B:SMS   |\n|----------|----------|\n");
}