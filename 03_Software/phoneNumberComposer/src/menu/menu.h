///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:           
//
///////////////////////////////////////////////////////////////////////////////
#ifndef MENU_ONCE
#define MENU_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include "../xf/event.h"
#include <drivers/gpio.h>
#include "../xf/ireactive.h"
#include <stdlib.h>
#include <list>
#include <string>

class Menu : public IReactive
{
public:
    void startBehaviour();
    bool processEvent(Event* e);
    void init();
    static Menu* getInstance();
    void displayMenu();
        typedef enum MENUSTATES {
	    ST_INIT
        ,ST_MENU
        ,ST_SMS
        ,ST_CALL
        } MENUSTATES;

        typedef enum menuEvID {
		evInitial
        ,evDefault
        ,evSMS
        ,evCall
        ,evMenu
	} menuEvID;
    MENUSTATES state;
    MENUSTATES getMenuState();
private:
    Event ev;
    Menu();
    ~Menu();
    static Menu theMenu;
};
#endif
