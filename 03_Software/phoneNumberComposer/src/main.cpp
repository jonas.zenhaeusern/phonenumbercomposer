///////////////////////////////////////////////////////////////////////////////
//                   
// Title:            Phone Number Composer
// Version:          1.2
//
// Author:           Jonas Zenhäusern
// Email:            jonas.zenhaeusern@lernende.bfo-vs.ch
// Company:          Hes-so Valais Wallis
// Date:             16.03.2022
//
///////////////////////////////////////////////////////////////////////////////

#include <zephyr.h>
#include "factory/factory.h"
#include "xf/xf.h"

void main(void)
{
    try
    {
        Factory::init();
        Factory::build();
        Factory::start();

        while(1)
        {
            k_sleep(K_MSEC(4));
            Factory::xf()->execute();
        }
    }
    catch(const std::exception& e)
    {
        printk("Error occured: %s \n", e.what());
        exit(EXIT_FAILURE);
    }
    

}
