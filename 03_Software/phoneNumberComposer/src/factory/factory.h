///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      initialized and starts all objects of the porject     
//
///////////////////////////////////////////////////////////////////////////////
#ifndef FACTORY_ONCE
#define FACTORY_ONCE

#include "../xf/xf.h" 
#include "../hw/button.h"
#include "../hw/keypad.h"
#include "../controller/buttoncontroller.h"
#include "../model/model.h"
#include "../hw/led.h"
#include "../validation/validation.h"
#include "../hw/uart.h"
#include "../fona/fona.h"
#include "../hw/longClick.h"
#include "../menu/menu.h"

class Factory
{
public:
    static void init();
    static void build();
    static void start();

    static Validation* theValidation(){return Validation::getInstance();}
    static XF* xf(){return XF::getInstance();}
    static KeyPad* theKeyPad(){return &_theKeypad;}
    static ButtonController* theButtonController(){return ButtonController::getInstance();}
    static Model* theModel(){return Model::getInstance();}
    static Button* buttonA1(){return &_buttonA1;}
    static Button* buttonA2(){return &_buttonA2;}
    static Button* buttonA3(){return &_buttonA3;}
    static Button* buttonA4(){return &_buttonA4;}
    static Button* buttonB1(){return &_buttonB1;}
    static Button* buttonB2(){return &_buttonB2;}
    static Button* buttonB3(){return &_buttonB3;}
    static Button* buttonB4(){return &_buttonB4;}
    static Button* buttonC1(){return &_buttonC1;}
    static Button* buttonC2(){return &_buttonC2;}
    static Button* buttonC3(){return &_buttonC3;}
    static Button* buttonC4(){return &_buttonC4;}
    static Button* buttonD1(){return &_buttonD1;}
    static Button* buttonD2(){return &_buttonD2;}
    static Button* buttonD3(){return &_buttonD3;}
    static Button* buttonD4(){return &_buttonD4;}
    static LED* ledGreen(){return &_ledGreen;}
    static LED* ledRed(){return &_ledRed;}
    static LED* ledYellow(){return &_ledYellow;}
    static Uart* uart(){return &_uart;}
    static Fona* fona(){return Fona::getInstance();}
    static LongCLick* longCLick(){return LongCLick::getInstance();}
    static Menu* menu(){return Menu::getInstance();}


private:
    Factory();
    ~Factory();

private:
    static KeyPad _theKeypad;
    static Button _buttonA1;
    static Button _buttonA2;
    static Button _buttonA3;
    static Button _buttonA4;
    static Button _buttonB1;
    static Button _buttonB2;
    static Button _buttonB3;
    static Button _buttonB4;
    static Button _buttonC1;
    static Button _buttonC2;
    static Button _buttonC3;
    static Button _buttonC4;
    static Button _buttonD1;
    static Button _buttonD2;
    static Button _buttonD3;
    static Button _buttonD4;
    static LED _ledGreen;
    static LED _ledRed;
    static LED _ledYellow;
    static Uart _uart;
};

#endif