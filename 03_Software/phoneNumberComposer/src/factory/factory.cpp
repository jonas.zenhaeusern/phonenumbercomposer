#include "factory.h"

KeyPad Factory::_theKeypad(6,4,12,8, "GPIOA");
LED Factory::_ledRed(0,"GPIOA");
LED Factory::_ledGreen(1,"GPIOA");
LED Factory::_ledYellow(5,"GPIOA");
Uart Factory::_uart("UART_1",115200);
Button Factory::_buttonA1;
Button Factory::_buttonA2;
Button Factory::_buttonA3;
Button Factory::_buttonA4;
Button Factory::_buttonB1;
Button Factory::_buttonB2;
Button Factory::_buttonB3;
Button Factory::_buttonB4;
Button Factory::_buttonC1;
Button Factory::_buttonC2;
Button Factory::_buttonC3;
Button Factory::_buttonC4;
Button Factory::_buttonD1;
Button Factory::_buttonD2;
Button Factory::_buttonD3;
Button Factory::_buttonD4;

Factory::Factory()
{ 

}

Factory::~Factory()
{

}

void Factory::init()
{
    //initalize all components
    xf()->init();
    theKeyPad()->init();
    ledGreen()->initHW();
    ledRed()->initHW();
    ledYellow()->initHW();
    theValidation()->init();
    buttonA1()->initHW("A", 1, 0,"GPIOB");
    buttonA2()->initHW("A", 2, 1,"GPIOB");
    buttonA3()->initHW("A", 3, 5,"GPIOB");
    buttonA4()->initHW("A", 4, 4,"GPIOB");
    buttonB1()->initHW("B", 1, 0,"GPIOB");
    buttonB2()->initHW("B", 2, 1,"GPIOB");
    buttonB3()->initHW("B", 3, 5,"GPIOB");
    buttonB4()->initHW("B", 4, 4,"GPIOB");
    buttonC1()->initHW("C", 1, 0,"GPIOB");
    buttonC2()->initHW("C", 2, 1,"GPIOB");
    buttonC3()->initHW("C", 3, 5,"GPIOB");
    buttonC4()->initHW("C", 4, 4,"GPIOB");
    buttonD1()->initHW("D", 1, 0,"GPIOB");
    buttonD2()->initHW("D", 2, 1,"GPIOB");
    buttonD3()->initHW("D", 3, 5,"GPIOB");
    buttonD4()->initHW("D", 4, 4,"GPIOB");
    theValidation()->setLeds(ledGreen(),ledRed());

    uart()->initHW();
    uart()->enableRXInterrupt();    //enable UART interrupt
    fona()->init();
    menu()->init();
}

void Factory::build()
{
    //connect all subjects with observers
    theKeyPad()->subscribe(buttonA1());
    theKeyPad()->subscribe(buttonA2());
    theKeyPad()->subscribe(buttonA3());
    theKeyPad()->subscribe(buttonA4());
    theKeyPad()->subscribe(buttonB1());
    theKeyPad()->subscribe(buttonB2());
    theKeyPad()->subscribe(buttonB3());
    theKeyPad()->subscribe(buttonB4());
    theKeyPad()->subscribe(buttonC1());
    theKeyPad()->subscribe(buttonC2());
    theKeyPad()->subscribe(buttonC3());
    theKeyPad()->subscribe(buttonC4());
    theKeyPad()->subscribe(buttonD1());
    theKeyPad()->subscribe(buttonD2());
    theKeyPad()->subscribe(buttonD3());
    theKeyPad()->subscribe(buttonD4());
    buttonA1()->subscribe(longCLick());
    buttonA2()->subscribe(longCLick());
    buttonA3()->subscribe(longCLick());
    buttonA4()->subscribe(longCLick());
    buttonB1()->subscribe(longCLick());
    buttonB2()->subscribe(longCLick());
    buttonB3()->subscribe(longCLick());
    buttonB4()->subscribe(longCLick());
    buttonC1()->subscribe(longCLick());
    buttonC2()->subscribe(longCLick());
    buttonC3()->subscribe(longCLick());
    buttonC4()->subscribe(longCLick());
    buttonD1()->subscribe(longCLick());
    buttonD2()->subscribe(longCLick());
    buttonD3()->subscribe(longCLick());
    buttonD4()->subscribe(longCLick());
    longCLick()->subscribe(theButtonController());
    uart()->subscribe(fona());
    fona()->subscribe(theButtonController());
    theValidation()->subscribe(theButtonController());
}

void Factory::start()
{   
    //start statemachines
    theKeyPad()->startBehaviour();
    theValidation()->startBehaviour();
    longCLick()->startBehaviour();
    buttonA1()->startBehaviour();
    buttonA2()->startBehaviour();
    buttonA3()->startBehaviour();
    buttonA4()->startBehaviour();
    buttonB1()->startBehaviour();
    buttonB2()->startBehaviour();
    buttonB3()->startBehaviour();
    buttonB4()->startBehaviour();
    buttonC1()->startBehaviour();
    buttonC2()->startBehaviour();
    buttonC3()->startBehaviour();
    buttonC4()->startBehaviour();
    buttonD1()->startBehaviour();
    buttonD2()->startBehaviour();
    buttonD3()->startBehaviour();
    buttonD4()->startBehaviour();
    theButtonController()->init();
    fona()->startBehaviour();
    menu()->startBehaviour();
}
