#include "keypad.h"

KeyPad::KeyPad(int pA, int pB, int pC, int pD, const char* port) :
	pinA(pA,port), pinB(pB,port), pinC(pC,port), pinD(pD,port)
{
	// @USER_BEGIN

	// @USER_END
}

void KeyPad::init()
{
	state = ST_INIT;
	ev.setTarget(this);
	ev.setDnd(1);	//set do not delete

	//turn off all pins
	pinA.setInitialOff();	
    pinB.setInitialOff();
    pinC.setInitialOff();
    pinD.setInitialOff();
	//initalize gpio pins
    pinA.initHW();
    pinB.initHW();
    pinC.initHW();
    pinD.initHW();
}

void KeyPad::startBehaviour()
{
	ev.setId(evInitial);
	XF::getInstance()->pushEvent(&ev);
}

void KeyPad::subscribe(IKeyPadObserver * keyPadObserver)
{
	keyPadObserverList.push_back(keyPadObserver);
}

void KeyPad::notify(const char* row)
{
	for(auto observer : keyPadObserverList)
    {
		observer->checkButton(row);
	}
}

bool KeyPad::processEvent(Event *e)
{
	KEYPADSTATES oldstate;

	oldstate = this->state;

	switch (this->state)
	{
		case ST_INIT:
		{
			if(e->getId() == evInitial)
			{
				this->state = ST_WAIT;
			}
			break;
		}
		case ST_WAIT:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_A;
			}
			break;
		}
		case ST_A:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_B;
			}
			break;
		}
		case ST_B:
		{			
			if(e->getId() == evTm10)
			{
				this->state = ST_C;
			}
			break;
		}
		case ST_C:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_D;	
			}
			break;
		}
		case ST_D:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_A;
			}
			break;
		}
	}
	if(oldstate != this->state)
	{
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_WAIT:
			{
				break;
			}
			case ST_A:
			{
				pinA.off(); 
				break;
			}
			case ST_B:
			{
				pinB.off();
				break;
			}
			case ST_C:
			{
				pinC.off();
				break;
			}
			case ST_D:
			{
				pinD.off();
				break;
			}
		}

		int t = 10;
		switch (this->state)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_WAIT:
			{
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_A:
			{
				pinA.on();
				notify("A");
				ev.setId(evTm10);	//set 10ms timeout event
				ev.setTarget(this);
				ev.setDelay(t);	
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_B:
			{
				pinB.on();
				notify("B");
				ev.setId(evTm10);	//set 10ms timeout event
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);				
				break;
			}
			case ST_C:
			{
				pinC.on();
				notify("C");
				ev.setId(evTm10);	//set 10ms timeout event
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_D:
			{
				pinD.on();
				notify("D");
				ev.setId(evTm10);	//set 10ms timeout event
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
		}
		return true;
	}
	return false;
}

// @USER_BEGIN
// @USER_END
