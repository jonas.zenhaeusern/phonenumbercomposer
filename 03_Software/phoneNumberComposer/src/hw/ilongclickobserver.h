///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      Interface to notify all longclickobserver             
//
///////////////////////////////////////////////////////////////////////////////

#ifndef ILONGCLICKOBSERVER_ONCE
#define ILONGCLICKOBSERVER_ONCE
#include <string>

class Event;

class ILongClickObserver
{
public:
    virtual void click(const char* _row, uint8_t id, uint8_t duration) = 0;
};

#endif