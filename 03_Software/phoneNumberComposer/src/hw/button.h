///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            Button
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      reactive class to check the state of a certain input pin 
//
///////////////////////////////////////////////////////////////////////////////
#ifndef BUTTON_ONCE
#define BUTTON_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include <drivers/gpio.h>
#include <stdlib.h>
#include "../gpio/gpi.h"
#include "IKeyPadobserver.h"
#include "ibuttonObserver.h"
#include <list>
#include <string>


class Button : public IReactive , public IKeyPadObserver
{
public:
    typedef enum POLLSTATE
    {
        ST_INITIAL,
        ST_WAIT,
        ST_POLL
    } POLLSTATE;
    typedef enum PINSTATE
    {
        LOW,
        HIGH
    } PINSTATE;
    Button();
    ~Button();
    void initHW(const char* _row, uint8_t _id, uint8_t pin, const char* port); //set button coordinates, gpio pin & port
    bool processEvent(Event* event);  //contains statemachine
    void startBehaviour(); //start statemachine
    bool pressed();
    void checkButton(const char* _row);
    void subscribe(IButtonObserver * buttonObserver);    

private: 
    Event pollTm;
    Event ev;
    POLLSTATE state;    //initalize pollstate object
    PINSTATE pinState;  //initalize pinstate object
    PINSTATE oldPinState;  
    const struct device* driver; 
    uint8_t pin;
    const char* row;
    uint8_t id;
    typedef enum buttonEvID {   //list of all available events
		evInitial
        ,evDefault
        ,evTimeout
		,evTm10
        ,evPoll
	} keypadEvID;
    std::list <IButtonObserver*> observerList;
    void notify(PINSTATE ps);
};
#endif