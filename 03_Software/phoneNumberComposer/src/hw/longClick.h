///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      class to identify the duration of a keyinput     
//
///////////////////////////////////////////////////////////////////////////////

#ifndef LONGCLICK_ONCE
#define LONGCLICK_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include "ibuttonobserver.h"
#include "ilongclickobserver.h"
#include <list>

class LongCLick : public IReactive , public IButtonObserver
{
public:
    void startBehaviour();  //start statemachine
    bool processEvent(Event* event);

    void pressed(const char* row, uint8_t id); 
    void released(const char* row, uint8_t id);

    typedef enum CLICKSTATE
    {
        ST_INITIAL,
        ST_WAIT,
        ST_PRESSED,
        ST_SHORT,
        ST_LONG
    } CLICKSTATE;
    typedef enum MODE
    {
        SHORT,
        LONG,
        REL
    }MODE;

    void subscribe(ILongClickObserver * longCLickObserver);
    static LongCLick* getInstance();
private:
    LongCLick();
    ~LongCLick();
    const char* tempRow;
    uint8_t tempId;
    typedef enum longClickEvID {    //all longCLick events
            evInitial
            ,evDefault
            ,evPressed
            ,evReleased
            ,evTimeout
        } keypadEvID;
    uint8_t id;
    static LongCLick theLongCLick;
    std::list <ILongClickObserver*> LongCLickObserverList;
    Event evLongTm;
    Event ev;
    CLICKSTATE state;
    void notify(MODE m);
};
#endif