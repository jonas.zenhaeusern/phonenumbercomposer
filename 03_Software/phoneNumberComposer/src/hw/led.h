///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      class to controll a light-emitting diode      
//
///////////////////////////////////////////////////////////////////////////////

#ifndef LED_ONCE
#define LED_ONCE

#include <zephyr.h>
#include "../gpio/gpo.h"

class LED
{
public:
    LED(int p1, const char* port);  //set gpio pin, port
    ~LED();
    void initHW();
    void on();
    void off();

private:
    GPO pin;
};
#endif