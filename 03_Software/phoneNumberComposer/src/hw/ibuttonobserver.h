///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      Interface to notify all buttonobserver     
//
///////////////////////////////////////////////////////////////////////////////
#ifndef IBUTTONOBSERVER_ONCE
#define IBUTTONOBSERVER_ONCE
#include <zephyr.h>
#include <string>

class Event;

class IButtonObserver
{
public:
    virtual void pressed(const char* row, uint8_t id) = 0;
    virtual void released(const char* row, uint8_t id) = 0;
};

#endif