///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      Interface to notify all keypadobserver       
//
///////////////////////////////////////////////////////////////////////////////
#ifndef IKEYPADPAD_ONCE
#define IKEYPADPAD_ONCE
#include <string>


class IKeyPadObserver
{
public:
    virtual void checkButton(const char* _row) = 0;
};

#endif