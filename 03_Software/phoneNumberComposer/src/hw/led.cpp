#include "led.h"

LED::LED(int p1, const char* port) : pin(p1, port)
{

}

LED::~LED()
{

}

void LED::initHW()
{
    pin.setInitialOff();
    pin.setLowPower();
    pin.initHW();
}

void LED::on()
{
    pin.on();
}

void  LED::off()
{
    pin.off();
}