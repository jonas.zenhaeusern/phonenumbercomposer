#include "LongcLick.h"
#include "../factory/factory.h"

LongCLick LongCLick::theLongCLick;

LongCLick::LongCLick()
{
    evLongTm.setDnd(true);  //set do not delete
    evLongTm.setId(evTimeout);
    evLongTm.setTarget(this);
    evLongTm.setDelay(500);

    ev.setDnd(true);
    ev.setTarget(this);

    state = ST_INITIAL;
}

LongCLick::~LongCLick()
{

}

void LongCLick::startBehaviour()
{
    ev.setId(evInitial);
    ev.setDelay(0);
    XF::getInstance()->pushEvent(&ev);
}

bool LongCLick::processEvent(Event* event)
{
    bool processed = false;
    CLICKSTATE oldstate = state;
    switch (state)
    {
        case ST_INITIAL:
        if (event->getId()==evInitial)
        {
            state = ST_WAIT;
        }
        break;
        case ST_WAIT:
        if (event->getId() == evPressed)
        {
            state = ST_PRESSED;
        }
        break;
        case ST_PRESSED:
        if(event->getId() == evReleased)
        {
            state = ST_SHORT;
        }
        else if (event->getId() == evTimeout)
        {
            state = ST_LONG;
        }       
        break;
        case ST_LONG:
        if(event->getId() == evReleased)
        {
            state = ST_WAIT;
        }
        break;
        case ST_SHORT:
        if(event->getId() == evDefault)
        {
            state = ST_WAIT;
        }
        break;
        default:
        break;
    }
    if (state != oldstate)
    {
        switch (state)
        {
            case ST_INITIAL:
            break;
            case ST_WAIT:
                if(oldstate == ST_LONG)
                {
                    notify(REL);
                }
            break;
            case ST_PRESSED:
                evLongTm.setDelay(500);
                Factory::xf()->pushEvent(&evLongTm);              
            break;
            case ST_LONG:
                notify(LONG);
            break;
            case ST_SHORT:
                notify(SHORT);
                Factory::xf()->unscheduleTM(&evLongTm);
                ev.setId(evDefault);
                Factory::xf()->pushEvent(&ev);
            break;
            default:
            break;
        }
        processed = true;
    }
    return processed;
}

void LongCLick::pressed(const char* row, uint8_t id)
{
    this->tempRow = row;
    this->tempId = id;
    ev.setId(evPressed);
    Factory::xf()->pushEvent(&ev);
}

void LongCLick::released(const char* row, uint8_t id)
{
    this->tempRow = row;
    this->tempId = id;
    ev.setId(evReleased);
    Factory::xf()->pushEvent(&ev);
}

void LongCLick::subscribe(ILongClickObserver * LongCLickObserver)
{
    LongCLickObserverList.push_back(LongCLickObserver);
}

LongCLick* LongCLick::getInstance()
{
    return &theLongCLick;
}

void LongCLick::notify(MODE m)
{
    for(auto observer : LongCLickObserverList)
    {
        switch (m)
        {
        case SHORT:
            observer->click(tempRow, tempId, 1);
            break;
        case LONG:
            observer->click(tempRow, tempId, 2);
            break;     
        default:
            break;
        }
    }
    this->tempRow = nullptr;
    this->tempId = NULL;
}