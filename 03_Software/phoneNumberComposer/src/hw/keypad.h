/////////////////////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      ractive class to activate different keypad row in a certain intervall     
//
/////////////////////////////////////////////////////////////////////////////////////////////

#ifndef KEYPAD_H
#define KEYPAD_H

#include <stdint.h>
#include "../xf/xf.h"
#include "../xf/event.h"
#include "../xf/IReactive.h"
#include "../gpio/gpo.h"
#include "ikeypadobserver.h"
#include <list>
#include <string>

// @USER_BEGIN
// @USER_END

class KeyPad : public IReactive
{
public:
	KeyPad(int pA, int pB, int pC, int pD, const char* port);
	void init();
	void startBehaviour();	//start statemachine
	bool processEvent(Event* e);
	typedef enum KEYPADSTATES {
	ST_INIT, ST_WAIT, ST_A, ST_B, ST_C, ST_D} KEYPADSTATES;
	void subscribe(IKeyPadObserver * keyPadObserver);

private:
	KEYPADSTATES state;
	typedef enum keypadEvID {	//keypad events
		evInitial
		,evTm10
	} keypadEvID;

	std::list <IKeyPadObserver*> keyPadObserverList;
	Event ev;
	GPO pinA;               /**< General Purpose Output >**/
    GPO pinB;               /**< General Purpose Output >**/
    GPO pinC;               /**< General Purpose Output >**/
    GPO pinD;               /**< General Purpose Output >**/
	void notify(const char* row);
};
#endif
