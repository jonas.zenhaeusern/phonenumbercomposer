#include "button.h"
#include "../xf/xf.h"
#include "../factory/factory.h"

Button::Button()
{
    pollTm.setDnd(true);    //set do not delete
    pollTm.setId(evTimeout);
    pollTm.setTarget(this);
    pollTm.setDelay(50);

    ev.setDnd(true);
    ev.setTarget(this);

    state = ST_INITIAL;
    pinState = LOW;
    oldPinState = LOW;
}

Button::~Button()
{

}
void Button::initHW(const char* _row, uint8_t _id, uint8_t p, const char* port)
{
    id = _id;
    row = _row;
    pin = p;
    
    int ret;
    driver = const_cast<device*> (device_get_binding(port));    //config device
    ret = gpio_pin_configure(driver,pin,GPIO_INPUT | GPIO_ACTIVE_LOW | GPIO_PULL_DOWN); //set pin to input, set pulldown mode
    pinState =  (PINSTATE) gpio_pin_get(driver,pin);
    oldPinState = pinState;
}
void Button::checkButton(const char* _row)
{
    if(row == _row)
    {
        ev.setId(evPoll);
        Factory::xf()->pushEvent(&ev);
    }
}

bool Button::processEvent(Event* event)
{
    bool processed = false;
    POLLSTATE oldstate = state;
    switch (state)
    {
        case ST_INITIAL:
        if (event->getId()==evInitial)
        {
            state = ST_WAIT;

        }
        break;
        case ST_WAIT:
        if (event->getId() == evPoll)
        {
            state = ST_POLL;
        }
        break;
        case ST_POLL:
        if (event->getId() == evDefault)
        {
            state = ST_WAIT;
        }
        break;
        default:
        break;
    }
    if (state != oldstate)
    {
        switch (state)
        {
            case ST_INITIAL:
            break;
            case ST_WAIT:
            break;
            case ST_POLL:
            pinState = (PINSTATE) gpio_pin_get(driver,pin); //get gpio pin status
            if (pinState == HIGH && oldPinState == LOW)
            {
                notify(LOW);
            }
            if (pinState == LOW && oldPinState == HIGH)
            {
                notify(HIGH);
            }
            if (pinState ==  oldPinState)
            {
                //no change
            }
            oldPinState = pinState;
            ev.setId(evDefault);
            Factory::xf()->pushEvent(&ev);
            break;
            default:
            break;
        }
        processed = true;
    }
    return processed;
}

void Button::startBehaviour()
{
    ev.setId(evInitial);
    Factory::xf()->pushEvent(&ev);
}

void Button::subscribe(IButtonObserver * buttonObserver)
{
    observerList.push_back(buttonObserver);
}

void Button::notify(PINSTATE ps)
{
    //notify all buttonobserver objects
    for (auto observer : observerList)
    {
        if(ps == HIGH)
        {
            observer->pressed(row, id);  
        }
        else
        {
            observer->released(row, id);
        }
    }
}