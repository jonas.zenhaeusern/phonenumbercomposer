#include "validation.h"
#include "../factory/factory.h"

Validation Validation::theValidation;

Validation::Validation()
{
	validationActive = 1;
	ndc = "(21|22|24|26|27|31|32|33|34|41|43|44|52|55|56|61|62|71|81|91|73|75|76|77|78|79)";
    emergencyNumbers = "(117|118|143|144|145)";
}
Validation::~Validation()
{
    
}

Validation* Validation::getInstance()
{
	return &theValidation;
}

Validation::VALIDATIONSTATES Validation::getvalidationState()
{
	return this->state;
}

void Validation::toggleValidation()
{
	if(validationActive)
	{
		printk("validation off\n");
		validationActive = false;
		ev.setId(evEmpty);
		ev.setTarget(this);
		XF::getInstance()->pushEvent(&ev);
	}
	else
	{
		printk("validation on\n");
		validationActive = true;
		checkStatus();
	}
}

void Validation::init()
{
    model = Factory::theModel();
	state = ST_INIT;
	ev.setTarget(this);
	ev.setDnd(1);
}

void Validation::setLeds(LED *_ledGreen, LED *_ledRed)
{
    ledGreen = _ledGreen;
    ledRed = _ledRed;
}

void Validation::startBehaviour()
{
	ev.setId(evInitial);
	XF::getInstance()->pushEvent(&ev);
}

void Validation::notify()
{
    for(auto observer : ValidationObserverList)
    {
    	observer->onValidation();
    }
}

void Validation::subscribe(IValidationObserver * validationObserver)
{
	ValidationObserverList.push_back(validationObserver);
}

void Validation::checkStatus()
{
	if(validationActive)
	{
		tempNumber = model->getCurrentInput();
		numSize = tempNumber.size(); //get size of enterd number
		if(tempNumber == "")
		{
			ev.setId(evEmpty);
		}
		else if(numSize == 1) //check 1 character sized numbers
		{
			if (regex_match (tempNumber, regex("([0-1]|\\+)") ) == false)	
			{
				ev.setId(evInvalid);			
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 2) //check 2 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+4|00|0[2-9]|11|14)") ) == false)
			{
				ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 3) //check 3 character sized numbers
		{
			if(regex_match (tempNumber, regex(emergencyNumbers) ) == true)
			{
				ev.setId(evValid);
			}
			else if (regex_match (tempNumber, regex("(\\+41|004|141|0"+ndc+")") ) == false)
			{
				ev.setId(evInvalid);
			}		
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 4) //check 4 character sized numbers
		{
			if (regex_match (tempNumber, regex("(1414)") ) == true)
			{
				ev.setId(evValid);
			}
			else if (regex_match (tempNumber, regex("(\\+41[2-9]|0041|0"+ndc+"[0-9])") ) == false)
			{
				ev.setId(evInvalid);
			}

			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 5) //check 5 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"|0041[2-9]|0"+ndc+"[0-9]{2})") ) == false)
			{
				ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 6) //check 6 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]|004179|0"+ndc+"[0-9]{3})") ) == false)
			{
				ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 7) //check 7 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{2}|0041"+ndc+"[0-9]|0"+ndc+"[0-9]{4})") ) == false)
		{
			ev.setId(evInvalid);
		}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 8) //check 8 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{3}|0041"+ndc+"[0-9]{2}|0"+ndc+"[0-9]{5})") ) == false)
		{
			ev.setId(evInvalid);
		}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 9) //check 9 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{4}|0041"+ndc+"[0-9]{3}|0"+ndc+"[0-9]{6})") ) == false)
			{
				ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 10) //check 10 character sized numbers
		{
			if(regex_match (tempNumber, regex("(0"+ndc+"[0-9]{7})") ) == true)
			{
				ev.setId(evValid);
			}
			else if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{5}|0041"+ndc+"[0-9]{4})") ) == false)
			{
				ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 11) //check 11 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{6}|0041"+ndc+"[0-9]{5})") ) == false)
		{
			ev.setId(evInvalid);
		}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 12) //check 12 character sized numbers
		{
			if (regex_match (tempNumber, regex("(\\+41"+ndc+"[0-9]{7})") ) == true)
			{
			ev.setId(evValid);
			}
			else if (regex_match (tempNumber, regex("(0041"+ndc+"[0-9]{6})") ) == false)
			{
			ev.setId(evInvalid);
			}
			else
			{
				ev.setId(evIncomplete);
			}
		}
		else if(numSize == 13) //check 13 character sized numbers
		{
			if (regex_match (tempNumber, regex("(0041"+ndc+"[0-9]{7})") ) == true)
			{
				ev.setId(evValid);
			}
			else
			{
				ev.setId(evInvalid);
			}	
		}
		else
		{
			ev.setId(evInvalid);
		}
		ev.setTarget(this);
		XF::getInstance()->pushEvent(&ev);
		tempNumber = "";
	}
}

bool Validation::processEvent(Event* e)
{
    VALIDATIONSTATES oldstate;

	oldstate = this->state;
	switch (this->state)
	{
		case ST_INIT:
		{
			if(e->getId() == evInitial)
			{
				this->state = ST_EMPTY;
			}
			break;
		}
		case ST_EMPTY:
		{
			if(e->getId() == evInvalid)
			{
				this->state = ST_INVALID;
				
			}
			else if(e->getId() == evIncomplete)
			{
				this->state = ST_INCOMPLETE;
				
			}
			break;
		}
		case ST_INCOMPLETE:
		{
			if(e->getId() == evValid)
			{
				this->state = ST_VALID;
				
			}
			else if(e->getId() == evInvalid)
			{
				this->state = ST_INVALID;
				
			}
			else if(e->getId() == evEmpty)
			{
				this->state = ST_EMPTY;
				
			}
			break;
		}
		case ST_VALID:
		{			
			if(e->getId() == evIncomplete)
			{
				this->state = ST_INCOMPLETE;
				
			}
			else if(e->getId() == evInvalid)
			{
				this->state = ST_INVALID;
				
			}
            else if(e->getId() == evEmpty)
			{
				this->state = ST_EMPTY;
				
			}
			break;
		}
		case ST_INVALID:
		{
		    if(e->getId() == evEmpty)
			{
				this->state = ST_EMPTY;
			}
			if(e->getId() == evIncomplete)
			{
				this->state = ST_INCOMPLETE;
			}
			if(e->getId() == evValid)
			{
				this->state = ST_VALID;
			}
			break;
		}
	}
    //on exit
	if(oldstate != this->state)
	{
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_EMPTY:
			{
				break;
			}
			case ST_INCOMPLETE:
			{

				break;
			}
			case ST_INVALID:
			{
				ledRed->off();
				break;
			}
			case ST_VALID:
			{
				ledGreen->off();
				break;
			}
		}
        //on entry
		switch (this->state)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_EMPTY:
			{
				break;
			}
			case ST_INCOMPLETE:
			{
				break;
			}
			case ST_INVALID:
			{
				ledRed->on();
				break;
			}
			case ST_VALID:
			{
				ledGreen->on();
			}
		}
		Factory::theButtonController()->onValidation();
		return true;
	}
	return false;
}