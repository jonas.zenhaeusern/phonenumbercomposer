///////////////////////////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:      reactive class that checks a swiss telephone number for its formal validity            
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef VALIDATION_ONCE
#define VALIDATION_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include <drivers/gpio.h>
#include <stdlib.h>
#include <list>
#include <string>
#include <bits/stdc++.h>
#include <iostream>
#include <iterator>
#include <xf/ireactive.h>
#include "../xf/event.h"
#include <regex>
#include "../hw/led.h"

class Model;


class Validation : public IReactive
{
    public:
    class IValidationObserver
        {
            public:
            virtual void onValidation() = 0;
        };
        void init();
        void startBehaviour();  //start statemachine
        bool processEvent(Event* e);
        void checkStatus(); //check validation status of current phonenumber
        void setLeds(LED *_ledGreen, LED *_ledRed); //set indication leds

        typedef enum VALIDATIONSTATES {
	    ST_INIT
        ,ST_EMPTY
        ,ST_INCOMPLETE
        ,ST_INVALID
        ,ST_VALID
        } VALIDATIONSTATES;

        typedef enum validationEvID {
		evInitial
        ,evDefault
        ,evEmpty
        ,evValid
        ,evIncomplete
        ,evInvalid
	} validationEvID;
    VALIDATIONSTATES state;
    VALIDATIONSTATES getvalidationState();
    void toggleValidation();    //turn validation on/off
    static Validation* getInstance();
    bool getValidationStatus();
    void subscribe(IValidationObserver * validationObserver);
    private:
        Validation();
        ~Validation();
        static Validation theValidation;
        bool validationActive;
        Event ev;
        Model* model;
        LED * ledGreen;
        LED * ledRed;
        string tempNumber;
        string ndc;
        string emergencyNumbers;
        int numSize;
        std::list <IValidationObserver*> ValidationObserverList;
        void notify();
};
#endif