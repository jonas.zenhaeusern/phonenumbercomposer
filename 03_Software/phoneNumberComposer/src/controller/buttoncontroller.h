///////////////////////////////////////////////////////////////////////////////
//                   
// Class:            
// Version:          1.2
// Author:           Jonas Zenhäusern
// Description:           
//
///////////////////////////////////////////////////////////////////////////////
#ifndef BUTTONCONTROLLER_ONCE
#define BUTTONCONTROLLER_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include "../fona/ifonaobserver.h"
#include <drivers/gpio.h>
#include <stdlib.h>
#include "../gpio/gpi.h"
#include "../hw/ilongclickobserver.h"
#include <list>
#include <string>
#include "../validation/validation.h"

class Model;
class Fona;
class Menu;

class ButtonController : public ILongClickObserver, public IFonaObserver, public Validation::IValidationObserver
{
public:
    void init();
    static ButtonController* getInstance();
    void click(const char* row, uint8_t id, uint8_t duration); //longclick notify
    void processKeyInput(string _input);    //handles all keypad inputs and assignes functionality to special key buttons
    void onValidation();                 //validation notify
    void onResponse(char* message);     //fona notify
    void deleteLastInput(string _input);
    void clearInput();
    void addInput(string _addedInput);
    void mapInput(string _input);

private: 
    ButtonController();
    ~ButtonController();
    string tempRow;     
    static ButtonController theButtonController;
    Model* model;       //pointer to access model class via factory
    Validation* validation;  //pointer to access validation class via factory
    Fona* fona;     //pointer to access fona class via factory
    Menu* menu;     //pointer to access menu class via factory
};
#endif
