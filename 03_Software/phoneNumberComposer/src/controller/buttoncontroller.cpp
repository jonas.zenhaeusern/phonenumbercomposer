#include "buttonController.h"
#include "../factory/factory.h"

ButtonController ButtonController::theButtonController;

ButtonController::ButtonController()
{
    model = Factory::theModel();
    validation = Factory::theValidation();
    fona = Factory::fona();
    menu = Factory::menu();
}
ButtonController::~ButtonController()
{
    
}

ButtonController* ButtonController::getInstance() {
    return &theButtonController;
}

void ButtonController::init()
{
    //fona->send("AT+CPIN=PIN");     //unlock sim 
}
void ButtonController::onResponse(char* message)
{

}

void ButtonController::click(const char* _row, uint8_t id, uint8_t duration)
{
    tempRow = _row;
    processKeyInput((tempRow + "," + to_string(id)) + "," + to_string(duration));
}

void ButtonController::clearInput()
{
    model->setCurrentInput("");
    printk("\n");
}

void ButtonController::addInput(string _addedInput)
{
    if(model->getCurrentInput().size() < 30)
    {
        model->setCurrentInput(model->getCurrentInput() + _addedInput);
        model->setTempKeyInput("");
        printk(model->getCurrentInput().data());
        printk("\n");
    }
}

void ButtonController::deleteLastInput(string _input)
{
    if(model->getCurrentInput().size() >= 1)
    {
        _input.pop_back();
        model->setCurrentInput(_input);
        model->setTempKeyInput("");
        printk(model->getCurrentInput().data());
        printk("\n");
    }
}

void ButtonController::mapInput(string _input)
{
    model->setTempKeyInput(model->getKeyMap(_input));
}

void ButtonController::processKeyInput(string _input)
{
    if(menu->getMenustate() == menu->ST_MENU)
    {
        
    }
    else if(menu->getMenustate() == menu->ST_CALL)
    {

    }
    else if(menu->getMenustate() == menu->ST_SMS)
    {

    }
    mapInput(_input);
    if(model->getTempKeyInput() == "A")     //assigning functionality to A-Button
    {
        if(fona->getFonaState() == fona->ST_RING)
        {
            fona->send("ATA");      //accept incomming call
        }
        else if(fona->getFonaState() == fona->ST_READY)
        {
            if(validation->getvalidationState() == validation->ST_VALID) 
            {
                fona->send("ATD" + model->getCurrentInput() +"i;"); //send call command to fona
                fona->initCall();
                clearInput();
                validation->checkStatus();
                printk("call started\n");
            }
            else
            {
                 printk("enter valid number\n");
            }
        }
        else if(fona->getFonaState() == fona->ST_CALL)
        {
            printk("call active\n");
        }
        else
        {
            printk("invalid number\n");
        }
    }
    else if(model->getTempKeyInput() == "B")    //assigning hang up functionality to B-Button
    {
        //check for all possible hang up cases
        if(fona->getFonaState() == fona->ST_CALL || fona->getFonaState() == fona->ST_INITCALL || fona->getFonaState() == fona->ST_RING) 
        {
            fona->send("AT+CHUP");
            fona->hangUp();
            printk("hang up\n");
        }
        else
        {
            printk("no active call\n");
        }
    }
    else if(model->getTempKeyInput() == "C")    //assigning functionality to C-Button
    {
        deleteLastInput(model->getCurrentInput());
        validation->checkStatus(); 
    }
    //assigning longclick functionalities 
    else if(model->getTempKeyInput() == "A_LONG")
    {
        fona->toggleAutoCall();         
    }
    else if(model->getTempKeyInput() == "B_LONG")
    {
    }
    else if(model->getTempKeyInput() == "C_LONG")
    {
        clearInput();            
        validation->checkStatus(); 
    }
    else if(model->getTempKeyInput() == "D_LONG")
    {
        validation->toggleValidation(); 
    }
    else
    {
        addInput(model->getTempKeyInput());          //adding non special key inputs to model
        validation->checkStatus();
    }
}

void ButtonController::onValidation()
{
    if(validation->getvalidationState() == validation->ST_VALID && fona->getAutoCallStatus()) //sending auto call command to fona if feature is enabled
    {
        fona->send("ATD" + model->getCurrentInput() +"i;");
        fona->initCall();
        clearInput();
    }
}