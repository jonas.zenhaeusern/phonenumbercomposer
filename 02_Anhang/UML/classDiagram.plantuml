@startuml
skinparam shadowing false
skinparam StateBackgroundColor none
skinparam StateBorderColor black
skinparam ArrowColor black
hide empty description
skinparam monochrome false
skinparam Ranksep 130
skinparam Nodesep 50
skinparam backgroundColor transparent
skinparam classbackgroundColor #cce1fc
skinparam classborderColor #000000
abstract class IReactive
abstract class IButtonObserver
abstract class IKeyPadObserver

Factory ..> XF 
XF --> Event
XF --|> IReactive
Factory..>Model
Factory..>KeyPad
Factory .right.> Button
Factory ..> Validation
Factory ..> ButtonController
Factory ..> Fona
Factory ..> LED
Factory ..> Uart
Button --|> IReactive
Button --|> IKeyPadObserver
ButtonController --|> IButtonObserver
Validation --|> IReactive
KeyPad --|> IReactive
Button ..> IButtonObserver
KeyPad ..> IKeyPadObserver
KeyPad ..> GPO
Button-->POLLSTATE
Button-->PINSTATE
Button-->buttonEvID
KeyPad-->KEYPADSTATES
Validation-->VALIDATIONSTATES
Validation-->validationEvID
ButtonController-->Model
Validation..>Model
Validation -->LED
LED..>GPO
Fona-->Uart
Fona-->fonaEvID
ButtonController --|> IFonaObserver
ButtonController --|> IValidationObserver
Fona --|> IUARTObserver
Uart ..> IUARTObserver
Validation ..> IValidationObserver
Fona ..> IFonaObserver


GPO-|>GPIO

class Factory {
--public Methods--
+{static}  init() : void
+{static}  build() : void
+{static}  start() : void
+{static}  xf() : XF* 
+{static}  theKeyPad() : KeyPad*
+{static}  theButtonController() : ButtonController*
+{static}  theModel() : Model*
+{static}  buttonA1() : Button*
+{static}  buttonA2() : Button*
+{static}  buttonA3() : Button*
+{static}  buttonA4() : Button*
+{static}  buttonB1() : Button*
+{static}  buttonB2() : Button*
+{static}  buttonB3() : Button*
+{static}  buttonB4() : Button*
+{static}  buttonC1() : Button*
+{static}  buttonC2() : Button*
+{static}  buttonC3() : Button*
+{static}  buttonC4() : Button*
+{static}  buttonD1() : Button*
+{static}  buttonD2() : Button*
+{static}  buttonD3() : Button*
+{static}  buttonD4() : Button*
+{static}  ledGreen() : LED*
+{static}  ledRed() : LED* 
+{static}  ledYellow() : LED* 
+{static}  theValidation() : Validation*  
+{static}  uart() : UART* 
+{static}  fona() : Fona*
+{static}  longCLick() : LongCLick*
--private Methods--
-Factory()
-~Factory()
__private Attributes__
-{static}  _theKeypad : KeyPad 
-{static}  _buttonA1 : Button
-{static}  _buttonA2 : Button
-{static}  _buttonA3 : Button
-{static}  _buttonA4 : Button
-{static}  _buttonB1 : Button
-{static}  _buttonB2 : Button
-{static}  _buttonB3 : Button
-{static}  _buttonB4 : Button
-{static}  _buttonC1 : Button 
-{static}  _buttonC2 : Button 
-{static}  _buttonC3 : Button 
-{static}  _buttonC4 : Button 
-{static}  _buttonD1 : Button 
-{static}  _buttonD2 : Button 
-{static}  _buttonD3 : Button 
-{static}  _buttonD4 : Button 
-{static} _theModel : Model 
-{static} _ledGreen : LED  
-{static} _ledRed : LED 
-{static} _ledYellow : LED   
-{static}  _uart : UART
}

Interface IReactive {
--public Methods--
+processEvent(Event* event) : virtual bool 
+startBehaviour() : void
}

class Event{
--public Methods--
+Event()
+Event(evID eventID)
+~Event()
+setTarget(IReactive* target) : void
+getTarget() : IReactive*
+setId(evID eventID) : void
+getId() : evID
__private Attributes__
-id : evID
-target : IReactive* 
}

class XF {
--public Methods--
+init() : void
+{static} getInstance() : XF*
+pushEvent(Event* e) : void
+execute() : void
+unscheduleTM(Event* e) : void
--private Methods--
-XF()
-~XF()
-popEvent() : Event*
-scheduleTM(Event* e) : void
-{static} void onTimeout(struct k_timer* t) : void
-{static} void onStop(struct k_timer* t) : void
__private Attributes__
-{static} theXF : XF
-evQueue : Queue<Event*> 	
}

class Button {
--public Methods--
+Button() : void
+initHW(_row, int _id, int pin, const char* port) : void
+processEvent(Event* event) : bool
+startBehaviour() : void
+pressed() : bool
+checkButton(string _row) : void
+subscribe(IButtonObserver * buttonObserver) : void
--private Methods--
-notify(PINSTATE ps) : void
__private Attributes__
-pollTm : Event
-ev : Event
-state : POLLSTATE
-pinState : PINSTATE
-oldPinState : PINSTATE
-driver : const struct device*
-pin : uint8_t
-row : const char*
-id : uint8_t
-observerList : list <IButtonObserver*>
}

enum POLLSTATE {
ST_INITIAL
ST_WAIT
ST_POLL
}

enum PINSTATE {
LOW
HIGH
}

enum buttonEvID {
evInitial
evDefault
evTimeout
evTm10
evPoll
}

enum fonaEvID {
evInitial
evOk
evDefault
evRing
evVoiceCallBeginn
evNoCarrier
evHangup
evInitCall
evPBDone
evError
evReset
}

class Fona {
--public Methods-- 
+processEvent(Event* e)
+startBehaviour() : void
+hangUp() : void
+initCall() : void
+{static} getInstance() : Fona*
+init
+subscribe(IFonaObserver* subscriber)  : void
+void send(string message)  : void
+toggleAutoCall() : void
+getAutoCall() : bool
+getFonaState() : FONASTATES
--private Methods--
-getFonaState() : FONASTATES
-Fona()
-~Fona()
-notify() : void
-onMessage(k_msgq* messages) : void
__private Attributes__
-UART* uart  
-state : FONASTATES
-ev : Event
-autoCall : bool
-{static} theFona : Fona
-subscribers : vector<IFonaObserver*>
-char data[MAXDATASIZE]
}

Interface IFonaObserver {
--public Methods--
+onResponse(char* message)= 0  : virtual void
}

Interface IValidationObserver {
--public Methods--
+onValidation()= 0  : virtual void
}

Interface IButtonObserver {
--public Methods--
+pressed(const char* row, int id) = 0  : virtual void
+released(const char* row, int id) = 0 : virtual void
}

Interface IKeyPadObserver {
--public Methods--
+checkButton(const char* _row) = 0 : virtual void
}

Interface IUARTObserver {
--public Methods--
+onMessage(k_msgq* messages) = 0: virtual void
}

class KeyPad {
--public Methods--
+KeyPad(int pA, int pB, int pC, int pD, const char* port)
+init() : void
+startBehaviour() : void
+processEvent(Event* e) : bool
+subscribe(IKeyPadObserver * keyPadObserver) : void

--private Methods--
-notify(const char* row) : void
__private Attributes__
-state : KEYPADSTATES
-keyPadObserverList : list <IKeyPadObserver*>
-ev : Event
-pinA : GPO
-pinB : GPO
-pinC : GPO
-pinD : GPO
}

enum KEYPADSTATES {
ST_INIT
ST_WAIT
ST_A
ST_B
ST_C
ST_D
}

class LED {
--public Methods--
+LED(int p1, const char* port) 
+~LED() 
+initHW() : void
+on() : void
+off() : void 
__private Attributes__
-pin : GPO
}

class Validation {
--public Methods-- 
+init() : void
+startBehaviour() : void
+processEvent(Event* e) : bool 
+checkStatus() : void
+setLeds(LED *_ledGreen, LED *_ledRed) : void
+checkStatus() : void
+getvalidationState() : VALIDATIONSTATES
+toggleValidation() : void
+{static}getInstance() : Validation*
+getValidationStatus() : bool
+subscribe(IValidationObserver * validationObserver) : void
--private Methods--
-Validation() 
-~Validation()
-notify() : void
__private Attributes__
-state : VALIDATIONSTATES 
-ledGreen : LED*
-ledRed : LED*
-Event ev 
-model : Model*
-tempNumber : string
-ndc : string
-emergencyNumbers : string
-numSize : int
-ValidationObserverList : <IValidationObserver*>
-{static}theValidation : Validation
}

enum VALIDATIONSTATES {
ST_INIT
ST_EMPTY
ST_INCOMPLETE
ST_INVALID
ST_VALID
}

enum validationEvID {
evInitial
evDefault
evEmpty
evValid
evIncomplete
evInvalid
}

class Model {
--public Methods--       
+getKeyMap(string _key) : string
+{static}getInstance() : Model*
+setTempKeyInput(string _currentInput) : void
+setCurrentInput(string _tempKeyInput) : void
+string getCurrentInput() 
+string getTempKeyInput() 
--private Methods--
-Model() 
-~Model()
__private Attributes__
-string currentInput 
-string tempKeyInput 
-{static}theModel : Model*
-keyMap : map<string, string> 
}

class ButtonController {
--public Methods--
+init() : void
+click(const char* row, uint8_t id, uint8_t duration) : void
+processKeyInput(string _input) : void
+processKeyInput(string _input) : void
+onValidation() : void
+onResponse(char* message) : void
+deleteLastInput(string _input) : void
+clearInput() : void
+addInput(string _addedInput) : void
+mapInput(string _input) : void
--private Methods--
-ButtonController()
-~ButtonController()
__private Attributes__
-tempRow : string
-theButtonController : ButtonController*
-Model* model
-Validation* validation
-Fona* fona
}

class Uart {
--public Methods--
+Uart(const char* deviceBinding, int baudrate)
+~Uart()
+initHW() : void
+uartSend(const char* txData) : void
+enableRXInterrupt() : bool
+subscribe(IUARTObserver* subscriber) : void
+processEvent(Event* e) : bool
--private Methods--
-setBaudrate(int baudrate) : void
-setEndOfMessage(uint8_t endOfMessage) : void
-notify() : void
__private Attributes__
-uart_dev : const struct device* 
-deviceBinding : const char* 
-baudrate : int
-endOfMessage : uint8_t
-message[MAXDATASIZE] : uint8_t
-pos : uint8_t
-endOfMessage : uint8_t
-subscribers : vector<IUARTObserver*>
-rp : Event
}

class GPIO {
--public Methods--
+GPIO(int p1, const char* port)
+virtual ~GPIO()
+void initHW()
+int getPin()
+int getUId()
+const struct device* getDriver()
__protected Attributes__
#id : uint8_t
#pin : int
#driver : const struct device* 
#port : char* 
#config : int
#{static}uid : uint8_t
}

class GPO {
--public Methods--
+GPO(int p1, const char* port)
+virtual ~GPO()
+setHighPower() : void
+setLowPower() : void
+setInitialOn() : void
+setInitialOff() : void
+setOpenDrain() : void
+setOpenSource() : void
+initHW() : void
+on() : void
+off() : void
+set(PINSTATE state)  : void
__protected Attributes__
#FORCE : enum
#MODE : enum
#initialState : PINSTATE                
#strength : FORCE
#mode : MODE
}

@enduml